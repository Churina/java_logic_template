package io.catalyte.training;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Contains multiple common logic exercises.
 */
public class LogicExercise {

  /**
   * Takes a BigDecimal for the unit price and an int for number of units sold and returns a
   * discounted sales total based on the quantity sold: if more than 99 units are sold, apply a 15%
   * discount to the total price; if more than 49 units are sold, apply a 10% discount 10% to the
   * total price; if less than 50 units are sold, do not apply a discount to the price. For example,
   * if the unit price was 1.00 and the quantity sold was 100, the method should return 85.00 for
   * the total sales amount.
   */
  public BigDecimal getDiscount(BigDecimal unitPrice, int unitAmount) {
    BigDecimal unitAmountBigDecimal = new BigDecimal(unitAmount);
    BigDecimal totalPrice = unitPrice.multiply(unitAmountBigDecimal);
    BigDecimal percent1 = BigDecimal.valueOf(1 - 0.15);
    BigDecimal percent2 = BigDecimal.valueOf(1 - 0.1);

    if(unitAmount > 99){
      BigDecimal totalPriceDiscount = totalPrice.multiply(percent1);
      String result = String.format("%.2f",totalPriceDiscount);
      return new BigDecimal(result);

    } else if(unitAmount > 49){
      BigDecimal totalPriceDiscount = totalPrice.multiply(percent2);
      String result = String.format("%.2f",totalPriceDiscount);
      return new BigDecimal(result);

    } else {
      String result = String.format("%.2f",totalPrice);
      return new BigDecimal(result);
    }
  }

  /**
   * This method takes an int representing a percentile score and should return the appropriate
   * letter grade. If the score is above 90, return 'A'; if the score is between 80 and 89, return
   * 'B'; if the score is between 70 and 79, return 'C'; if the score is between 60 and 69, return
   * 'D'; if the score is below 60, return 'F'.
   */
  public char getGrade(int score) {
    if(score >= 90){
      return 'A';
    } else if (score >= 80) {
      return 'B';
    } else if (score >= 70) {
      return 'C';
    } else if (score >= 60) {
      return 'D';
    } else {
      return 'F';
    }
  }

  /**
   * This method should take an ArrayList of strings, remove all the elements in the array
   * containing an even number of letters, and then return the result. For example, if given an
   * array of "Cat", "Dog", "Bird", the method should return an array containing only "Cat" and
   * "Dog".
   */

  public ArrayList<String> removeEvenLength(ArrayList<String> a) {
    for(int i = 0; i < a.size(); i++){
      if(a.get(i).length() % 2 == 0){
        a.remove(i);
        i--;
      }
    }return a;
  }


  /**
   * This method should take an double array, a, and return a new array containing the square of
   * each element in a.
   */
  public double[] powerArray(double[] a) {
    for(int i = 0; i < a.length; i++){
      a[i] = Math.pow(a[i],2);
    }
    return a;
  }


  /**
   * This method should take an int array, a, and return the index of the element with the greatest
   * value.
   */
  public int indexOfMax(int[] a) {
    if(a.length == 0) {
      return -1;
    }
    int max = a[0];
    int index = 0;
    for(int i = 0; i < a.length; i++){
      if(max < a[i]){
        max = a[i];
        index = i;
      }
    }
    return index;
  }


  /**
   * This method should take an ArrayList of Integers, a, and returns true if all elements in the
   * array are divisible by the given int, i.
   */
  public boolean isDivisibleBy(ArrayList<Integer> a, int i) {
    for (Integer integer : a) {
      if (integer % i != 0) {
        return false;
      }
//      for(int j = 0; j<a.size(); j++){
//        if(a.get(j) % i != 0){
//          return false;
//        }
    }
    return true;
  }

  /**
   * A word is "abecedarian" if its letters appear in alphabetical order--the word 'biopsy' for
   * example. This method should take String s and return true if it is abecedarian.
   */
  public boolean isAbecedarian(String s) {
    for(int i =1; i < s.length(); i++){
      if(s.charAt(i-1) >= s.charAt(i))
      return false;
    }
    return true;

//    for(int i =0; i < s.length(); i++){
//      return s.charAt(i) < s.charAt(i + 1);
//    }
//    return false;
  }

  /**
   * This method should take 2 strings and return true if they are anagrams of each other. For
   * example, "stop" is an anagram for "pots".
   */
  public boolean areAnagrams(String s1, String s2)
  {
    if(s1.length() != s2.length())
    {
      return false;
    } else
    {
      char[] ArrayS1 = s1.toLowerCase().toCharArray();
      char[] ArrayS2 = s2.toLowerCase().toCharArray();
      Arrays.sort(ArrayS1);
      Arrays.sort(ArrayS2);
      return Arrays.equals(ArrayS1,ArrayS2);
//      boolean status = Arrays.equals(ArrayS1,ArrayS2);
//      return status;
    }
  }

  /**
   * This method should take a String and return the number of unique characters. For example, if
   * the method is given "noon", it should return a value of 2.
   */

    public int countUniqueCharacters(String s) {
    ArrayList<Character> unique = new ArrayList<>();
    for( int i = 0; i < s.length(); i++){
      if(!unique.contains(s.charAt(i))){
        unique.add(s.charAt(i));
      }
    }
    return unique.size();
  }

  /**
   * This method should take a string and return true if it is a palindrome, i.e. it is spelled the
   * same forwards and backwards. For example, the words "racecar" and "madam" are palindromes.
   */
  public boolean isPalindrome(String s) {
    StringBuilder reverse = new StringBuilder();
    for(int i = s.length()-1; i>=0; i--){
      reverse.append(s.charAt(i));
    }
    return s.equals(reverse.toString());
//    boolean status = s.equals(reverse);
//    if(status){
//      return true;
//    } else {
//      return false;
//    }
  }

  /**
   * This method should take a string and return a HashMap which is a map of characters to a list of
   * their indices in a string (i.e., which characters occur where in a string). For example for the
   * string "Hello World", the map would look something like: d=[9], o=[4, 6], r=[7], W=[5], H=[0],
   * l=[2, 3, 8], e=[1].
   */
  public HashMap<String, ArrayList<Integer>> concordanceForString(String s) {
    HashMap<String, ArrayList<Integer>> map = new HashMap<>(); // Create map of respective keys and values
    for(int i = 0; i < s.length(); i++){
      ArrayList<Integer> indexes = new ArrayList<>(); // empty list of indexes
      char ch = s.charAt(i); // character of string at particular position
      if(map.containsKey(Character.toString(ch))){   //if key is already present in the map, then add the previous index associated with the character to the indexes list
      indexes.addAll(map.get(Character.toString(ch)));  //adding previous indexes to the list
      }
      indexes.add(i);  // add the current index of the character to the respective key in map
      map.put(Character.toString(ch),indexes);  // put the indexes in the map and map it to the current character
    }
    return map;
  }
}